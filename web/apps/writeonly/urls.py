from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('v1/writeonly/add-tracking', views.add_tracking, name="add-tracking"),
]

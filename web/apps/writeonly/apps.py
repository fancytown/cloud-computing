from django.apps import AppConfig


class WriteonlyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.writeonly'

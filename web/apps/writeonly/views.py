from django.shortcuts import render
from django.http import HttpResponse
from .models import WebsiteUser


# nginx port: 8080
def index(request):
    return HttpResponse('Write app Welcome Page')


# ports: 8001, 8002, 8003
def add_tracking(request):
    new_device = WebsiteUser()
    latitude = request.GET.get('latitude')
    longitude = request.GET.get('longitude')
    new_device.latitude = latitude
    new_device.longitude = longitude
    new_device.save()
    return HttpResponse("Added Latitude: " + str(latitude) + "\t" + "Added Longitude: " + str(longitude) + "\n")

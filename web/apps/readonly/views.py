from django.shortcuts import render
from django.http import HttpResponse
from . models import WebsiteUser
from django.template import loader

# nginx port: 8080
def index(request):
    # return HttpResponse('Read app Welcome Page')
    all_objects = WebsiteUser.objects.all()
    lat = WebsiteUser.latitude
    lng = WebsiteUser.longitude
    # template = loader.get_template('readonly/index.html')
    context = {'context': all_objects,
               'latitude': lat,
               'longitude': lng}
    # return HttpResponse(template.render(context, request))
    return 'hello read app'

# ports 7001, 7002, 7003
def last_tracking_by_device(request):
    all_objects = WebsiteUser.objects.all()
    latitude = WebsiteUser.objects.latest('latitude')
    longitude = WebsiteUser.objects.latest('longitude')
    context = {'all_data': all_objects,
               'long': longitude,
               'lat': latitude}
    return render(request, 'readonly/index.html', context)


